package com.example.heaven.worldtravel.service;

import com.example.heaven.worldtravel.data.model.UserData;

import io.reactivex.Flowable;

public interface ServiceLoadUserData extends CommonService {

    Flowable<UserData> getUser();

    void updateUser(UserData user);

    void deleteUser(UserData userData);

    void updateUserPosition(double lat, double lng, long time);
}
