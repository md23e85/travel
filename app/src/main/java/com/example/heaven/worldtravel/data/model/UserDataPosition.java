package com.example.heaven.worldtravel.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class UserDataPosition extends CommonModel {

    @Ignore
    public UserDataPosition() {
    }

    public UserDataPosition(double lat, double lng, long lastTimeUpdate) {
        this.lat = lat;
        this.lng = lng;
        this.lastTimeUpdate = lastTimeUpdate;
    }

    @PrimaryKey(autoGenerate = true)
    public int id;

    public double lat;

    public double lng;

    public long lastTimeUpdate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getLastTimeUpdate() {
        return lastTimeUpdate;
    }

    public void setLastTimeUpdate(long lastTimeUpdate) {
        this.lastTimeUpdate = lastTimeUpdate;
    }
}
