package com.example.heaven.worldtravel.service.google_service.place;

import com.example.heaven.worldtravel.service.CommonService;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

public interface ServiceLoadPlace extends CommonService {
    Observable<ResponseBody> queryForSearchPlace(String query, String radius);
}
