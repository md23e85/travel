package com.example.heaven.worldtravel.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.heaven.worldtravel.data.model.PlaceData;
import com.example.heaven.worldtravel.data.model.UserData;
import com.example.heaven.worldtravel.data.model.UserDataPosition;

import java.util.List;

@Dao
public interface UserDAO {

    @Insert
    void insert(UserData userData);

    @Update
    void updateUser(UserData userData);

    @Update
    void updateCurrentPosition(UserDataPosition position);

    @Delete
    void delete(UserData userData);

    @Query("SELECT * FROM userdata")
    UserData getUser();

    @Query("SELECT * FROM placedata")
    List<PlaceData> getPlaceData();

}
