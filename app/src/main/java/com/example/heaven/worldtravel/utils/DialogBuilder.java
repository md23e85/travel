package com.example.heaven.worldtravel.utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.heaven.worldtravel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action0;

public class DialogBuilder extends DialogFragment {

    private String title;
    private String content;
    private Action0 actionOk;
    private Action0 actionCancel;
    private boolean isInfo;

    @BindView(R.id.dialog_title)
    protected TextView dialogTitle;
    @BindView(R.id.dialog_content)
    protected TextView dialogContent;
    @BindView(R.id.dialog_ok)
    protected Button btnOk;
    @BindView(R.id.dialog_cancel)
    protected Button btnCancel;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_builder, null);
        builder.setView(view);
        ButterKnife.bind(this, view);
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        dialogContent.setText(content);
        dialogTitle.setText(title);
        btnOk.setOnClickListener(v -> {
            if(actionOk != null) actionOk.call();
            dismiss();
        });
        btnCancel.setOnClickListener(v -> {
            if(actionCancel != null) actionCancel.call();
            dismiss();
        });
        if (isInfo) {
            btnCancel.setVisibility(View.GONE);
            setCancelable(true);
        }
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    public DialogBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    public DialogBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public DialogBuilder setActionOk(Action0 actionOk) {
        this.actionOk = actionOk;
        return this;
    }

    public DialogBuilder setActionCancel(Action0 actionCancel) {
        this.actionCancel = actionCancel;
        return this;
    }

    public DialogBuilder isInfo(boolean isInfo) {
        this.isInfo = isInfo;
        return this;
    }

}
