package com.example.heaven.worldtravel;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.heaven.worldtravel.data.DB;
import com.example.heaven.worldtravel.injection.DaggerMainComponent;
import com.example.heaven.worldtravel.injection.MainComponent;
import com.example.heaven.worldtravel.injection.MainModule;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class App extends Application {

    private static MainComponent component;
    private static Cicerone<Router> cicerone;
    private static DB db;

    @Override
    public void onCreate() {
        super.onCreate();
        component = build();
        initCicerone();
        initDB();
    }

    public static MainComponent getComponent() {
        return component;
    }

    public static Router getCicerone() {
        return cicerone.getRouter();
    }

    public static NavigatorHolder getNavigator() {
        return cicerone.getNavigatorHolder();
    }

    public static DB getDb() {
        return db;
    }
    private MainComponent build() {
        return DaggerMainComponent
                .builder()
                .mainModule(new MainModule(getApplicationContext()))
                .build();
    }

    private void initCicerone() {
        cicerone = Cicerone.create();
    }

    private void initDB() {
        db =  Room.databaseBuilder(getApplicationContext(),
                DB.class, "database").fallbackToDestructiveMigration().build();
    }
}
