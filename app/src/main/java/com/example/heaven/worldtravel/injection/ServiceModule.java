package com.example.heaven.worldtravel.injection;

import com.example.heaven.worldtravel.service.ServiceLoadUserData;
import com.example.heaven.worldtravel.service.ServiceLoadUserDataImpl;
import com.example.heaven.worldtravel.service.google_service.place.ServiceLoadPlace;
import com.example.heaven.worldtravel.service.google_service.place.ServiceLoadPlaceImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Provides
    public ServiceLoadUserData provideServiceLoadUserData() {
        return new ServiceLoadUserDataImpl();
    }

    @Provides
    public ServiceLoadPlace provideServiceLoadPlace() {
        return new ServiceLoadPlaceImpl();
    }
}
