package com.example.heaven.worldtravel.data.converter;

import android.arch.persistence.room.TypeConverter;

import com.example.heaven.worldtravel.data.model.UserDataPosition;
import com.google.gson.Gson;

public class UserDataPositionConverter {
    private static Gson gson = new Gson();

    @TypeConverter
    public static UserDataPosition stringToObject(String string) {
        return gson.fromJson(string, UserDataPosition.class);
    }
    @TypeConverter
    public static String objectToString(UserDataPosition object) {
        return gson.toJson(object);
    }
}
