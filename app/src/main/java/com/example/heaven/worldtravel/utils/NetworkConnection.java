package com.example.heaven.worldtravel.utils;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;


public class NetworkConnection extends IntentService {

    public NetworkConnection() {
        super("undefined");
    }
    public NetworkConnection(String name) {
        super(name);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        while (true) {
            sendOnline(isOnline());
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() == null) return false;
        return true;
    }

    private void sendOnline(boolean isOnline) {
        Intent intent = new Intent("action");
        intent.putExtra("isOnline", isOnline);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
