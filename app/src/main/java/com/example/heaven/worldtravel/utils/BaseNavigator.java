package com.example.heaven.worldtravel.utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;

import com.example.heaven.worldtravel.Screen;
import com.example.heaven.worldtravel.fragment.MapSearchFragment;

import ru.terrakok.cicerone.android.AppNavigator;


public class BaseNavigator extends AppNavigator {

    public BaseNavigator(Activity activity, int containerId) {
        super(activity, containerId);
    }

    @Override
    protected Intent createActivityIntent(Context context, String screenKey, Object data) {
        return null;
    }

    @Override
    protected Fragment createFragment(String screenKey, Object data) {
        switch (screenKey) {
            case Screen.MAIN_SCREEN: return new MapSearchFragment();
            default:return null;
        }
    }
}
