package com.example.heaven.worldtravel.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.example.heaven.worldtravel.data.converter.UserDataPositionConverter;
import com.example.heaven.worldtravel.data.dao.PlaceDAO;
import com.example.heaven.worldtravel.data.dao.UserDAO;
import com.example.heaven.worldtravel.data.model.PlaceData;
import com.example.heaven.worldtravel.data.model.UserData;
import com.example.heaven.worldtravel.data.model.UserDataPosition;

@Database(entities = {UserData.class, PlaceData.class, UserDataPosition.class}, version = 4)
@TypeConverters(UserDataPositionConverter.class)
public abstract class DB extends RoomDatabase {
    public abstract UserDAO userDao();
    public abstract PlaceDAO placeDao();
}
