package com.example.heaven.worldtravel.service.google_service.place;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ServicePlace {
    String apiKey = "AIzaSyBUQr2nAiId52bTinyvDVXbGCINMlJmI3Q";

    @GET
    Observable<ResponseBody> getQueryPlace(@Url String search);
}
