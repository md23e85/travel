package com.example.heaven.worldtravel.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.example.heaven.worldtravel.data.converter.UserDataPositionConverter;

@Entity
public class UserData extends CommonModel{

    @PrimaryKey(autoGenerate = true)
    public int id;

    @TypeConverters(UserDataPositionConverter.class)
    public UserDataPosition currentUserPosition;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserDataPosition getCurrentUserPosition() {
        return currentUserPosition;
    }

    public void setCurrentUserPosition(UserDataPosition currentUserPosition) {
        this.currentUserPosition = currentUserPosition;
    }
}
