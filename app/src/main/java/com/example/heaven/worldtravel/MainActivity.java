package com.example.heaven.worldtravel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;


public class MainActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getComponent().inject(this);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, NavigatorActivity.class);
        startActivity(intent);
        finish();
    }

}
