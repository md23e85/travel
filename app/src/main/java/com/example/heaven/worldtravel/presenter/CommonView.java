package com.example.heaven.worldtravel.presenter;

import com.arellomobile.mvp.MvpView;

public interface CommonView extends MvpView {
    void showSnackBar(String text);
}
