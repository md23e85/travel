package com.example.heaven.worldtravel.service.google_service.place;

import com.example.heaven.worldtravel.data.model.UserDataPosition;
import com.example.heaven.worldtravel.service.CommonServiceImpl;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class ServiceLoadPlaceImpl extends CommonServiceImpl implements ServiceLoadPlace {

    @Override
    public Observable<ResponseBody> queryForSearchPlace(String query, String radius) {
        return Observable.just(db)
                .flatMap(db -> getRetrofit().create(ServicePlace.class).getQueryPlace(getUrl(query, radius, db.userDao().getUser().currentUserPosition))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())).subscribeOn(Schedulers.io());

    }

    private String getUrl(String search, String radius, UserDataPosition position) {
        return "https://maps.googleapis.com/maps/api/place/textsearch/json" +
                "?query="+search+"" +
                "&location="+position.getLat()+","+position.getLng() +
                "&radius="+radius +
                "&key=AIzaSyBUQr2nAiId52bTinyvDVXbGCINMlJmI3Q";
    }

}
