package com.example.heaven.worldtravel.presenter.map;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.example.heaven.worldtravel.App;
import com.example.heaven.worldtravel.presenter.CommonPresenter;
import com.example.heaven.worldtravel.service.ServiceLoadUserData;
import com.example.heaven.worldtravel.service.google_service.place.ServiceLoadPlace;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.observers.DefaultObserver;
import okhttp3.ResponseBody;

@InjectViewState
public class MapSearchPresenter extends CommonPresenter<MapSearchView> implements LocationListener {

    private LocationManager locationManager;
    private static final int ZOOM = 16;

    @Inject
    ServiceLoadUserData userService;

    @Inject
    ServiceLoadPlace placeService;

    public MapSearchPresenter() {
        App.getComponent().inject(this);
    }

    public void place(String search) {
        placeService.queryForSearchPlace(search, "10000").subscribe(new DefaultObserver<ResponseBody>() {
            @Override
            public void onNext(ResponseBody responseBody) {
                try {
                    String err = responseBody.string();
                    JSONObject jsonObject = new JSONObject(err);
                    Log.d(jsonObject.toString(), err);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(e.getLocalizedMessage(),"error");
            }

            @Override
            public void onComplete() {
                Log.d("0","error");
            }
        });
    }

    public void getCurrentLocationUser(Context context) {
        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) getViewState().setLocationUser(location, ZOOM);
    }

    @Override
    public void onLocationChanged(Location location) {
        userService.updateUserPosition(location.getLatitude(), location.getLongitude(), location.getTime());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
