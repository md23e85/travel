package com.example.heaven.worldtravel.injection;

import com.example.heaven.worldtravel.MainActivity;
import com.example.heaven.worldtravel.NavigatorActivity;
import com.example.heaven.worldtravel.presenter.map.MapSearchPresenter;
import com.example.heaven.worldtravel.service.CommonService;

import dagger.Component;

@Component(modules = {MainModule.class, DaoModule.class, ServiceModule.class, PresenterModel.class})
public interface MainComponent {
    void inject(MainActivity mainActivity);
    void inject(CommonService service);
    void inject(NavigatorActivity navigatorActivity);
    void inject(MapSearchPresenter mapSearchPresenter);
}
