package com.example.heaven.worldtravel.injection;

import com.example.heaven.worldtravel.presenter.map.MapSearchPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModel {

    @Provides
    public MapSearchPresenter provideMapSearchPresenter() {
        return new MapSearchPresenter();
    }
}
