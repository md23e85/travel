package com.example.heaven.worldtravel.presenter;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.example.heaven.worldtravel.App;

import ru.terrakok.cicerone.Router;

public class CommonPresenter<T extends MvpView> extends MvpPresenter<T>{

    private Router router = App.getCicerone();

    protected void navigateTo(String screen) {
        router.navigateTo(screen);
    }

    protected void navigateTo(String screen, Object... object) {
        router.navigateTo(screen, object);
    }

    protected void onBack(String screen) {
        router.backTo(screen);
    }

}
