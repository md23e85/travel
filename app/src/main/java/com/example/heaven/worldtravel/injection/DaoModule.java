package com.example.heaven.worldtravel.injection;

import com.example.heaven.worldtravel.App;
import com.example.heaven.worldtravel.data.dao.UserDAO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class DaoModule {

    @Singleton
    @Provides
    UserDAO provideUserDAO() {
        return App.getDb().userDao();
    }
}
