package com.example.heaven.worldtravel.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Update;

import com.example.heaven.worldtravel.data.model.PlaceData;
import com.example.heaven.worldtravel.data.model.UserData;

@Dao
public interface PlaceDAO {

    @Insert
    void insert(PlaceData... placeData);

    @Update
    void update(PlaceData placeData);

    @Delete
    void delete(PlaceData... placeData);
}
