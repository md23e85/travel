package com.example.heaven.worldtravel.presenter.map;

import android.location.Location;

import com.example.heaven.worldtravel.presenter.CommonView;


public interface MapSearchView extends CommonView {
    void setLocationUser(Location location, int zoom);
}
