package com.example.heaven.worldtravel.service;

import com.example.heaven.worldtravel.data.DB;
import com.example.heaven.worldtravel.data.model.UserData;
import com.example.heaven.worldtravel.data.model.UserDataPosition;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

public class ServiceLoadUserDataImpl extends CommonServiceImpl implements ServiceLoadUserData {


    @Override
    public Flowable<UserData> getUser() {
        return null;
    }

    @Override
    public void updateUser(UserData user) {

    }

    @Override
    public void deleteUser(UserData userData) {

    }

    @Override
    public void updateUserPosition(double lat, double lng, long time) {
        Observable.just(db)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new DefaultObserver<DB>() {
            @Override
            public void onNext(DB db) {
                if(db.userDao().getUser() == null) {
                    UserData userData = new UserData();
                    userData.currentUserPosition = new UserDataPosition(lat, lng, time);
                    db.userDao().insert(userData);
                } else {
                    db.userDao().updateCurrentPosition(new UserDataPosition(lat, lng, time));
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

}
