package com.example.heaven.worldtravel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.heaven.worldtravel.utils.BaseNavigator;
import com.example.heaven.worldtravel.utils.DialogBuilder;
import com.example.heaven.worldtravel.utils.NetworkConnection;

import ru.terrakok.cicerone.android.AppNavigator;

import static com.example.heaven.worldtravel.Constants.REQUEST_CODE_PERMISSION_LOCATION;
import static com.example.heaven.worldtravel.Constants.REQUEST_DENIED;


public class NavigatorActivity extends AppCompatActivity {

    protected RelativeLayout networkConnect;

    private AppNavigator navigator = new BaseNavigator(this, R.id.container);
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           if ( intent.getBooleanExtra("isOnline", false)) {
               networkConnect.setVisibility(View.GONE);
           } else {
               networkConnect.setVisibility(View.VISIBLE);
           }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        App.getNavigator().setNavigator(navigator);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigate);
        networkConnect = findViewById(R.id.network_container);
        getPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
        App.getComponent().inject(this);
        App.getCicerone().navigateTo(Screen.MAIN_SCREEN);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("action"));
        Intent intent = new Intent(this, NetworkConnection.class);
        startService(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults[0] == REQUEST_DENIED) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_PERMISSION_LOCATION:
                    new DialogBuilder()
                            .setTitle("Разрешение")
                            .isInfo(true)
                            .setContent("Для корректной работы приложения необходимо предоставить доступ к текущему местоположению, чтобы мы могли найти для вас только лучший заведения в Вашем городе:)")
                            .setActionOk(() -> getPermission(android.Manifest.permission.ACCESS_FINE_LOCATION))
                            .show(getFragmentManager(), this.getClass().getName());
                    break;
                default:
            }
        } else {

        }
    }

    private void getPermission(String access) {
        switch (access){
            case android.Manifest.permission.ACCESS_FINE_LOCATION:
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSION_LOCATION);
            default:
        }
    }


}
