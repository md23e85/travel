package com.example.heaven.worldtravel.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface UserPositionDAO {

    @Update
    void setLat(double lat);

    @Update
    void setLng(double lng);

    @Update
    void setLastTime(String time);

    @Query("SELECT * FROM userdataposition")
    double getUserPosition();
}
